FROM mcr.microsoft.com/playwright:v1.36.0-jammy

WORKDIR /app

COPY . .

RUN npm i pnpm http-server -g --registry http://registry.npmmirror.com
RUN pnpm i --registry http://registry.npmmirror.com
RUN pnpm run build

EXPOSE 8080

CMD ["node","main.js"]
