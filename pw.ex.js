import playwright from 'playwright';
import fs from 'fs'
const browser = await playwright.chromium.launch({
    headless:true
});
const context = await browser.newContext();
const page = await context.newPage();
await page.goto('https://www.trip.com/flights/');

page.evaluate(()=>{
    let script = document.createElement('script');
    script.src = 'https://cdn.jsdelivr.net/npm/rrweb@latest/dist/rrweb.min.js';
    document.getElementsByTagName('body')[0].appendChild(script);
    const events = []
    // 给一秒钟加载rrweb
    setTimeout(()=>{
        let stopFn = rrweb.record({
            emit(event) {
                events.push(event)
                localStorage.setItem('rrweb_events',JSON.stringify(events))
                if (events.length > 200) {
                    stopFn();
                }
            },
        });
    },2000)
})
await page.waitForTimeout(2200)

// ====================start==================


// 查找输入框元素
const inputBox = await page.$('input');
await inputBox.type('NYC',{delay:900});
await page.waitForTimeout(1000)


// ====================end==================
const rrwebEventsJson = await page.evaluate(()=>{
    return localStorage.getItem('rrweb_events')
})
await fs.writeFileSync('rrweb_events.json', rrwebEventsJson);
await page.screenshot({ path: 'example.png' });
await browser.close();