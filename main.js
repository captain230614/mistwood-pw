import express from 'express'
import fs from "fs";
import {exec} from 'child_process'
import bodyParser from 'body-parser'
const app = express()
// support parsing of application/json type post data
app.use(bodyParser.json());

//support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({ extended: true }));

import cors from 'cors'
app.use(cors())
app.post('/run',(req, res, next)=>{
    const code = req.body.code
    // 将数据写入文件
    fs.writeFile('pw.js', code, (err) => {
        if (err) throw err;
        exec('node pw.js', (err, stdout, stderr) => {
            console.log(err,stdout,stderr)
            if(err) {
                res.send({err,stdout,stderr})
            } else {
                fs.readFile('rrweb_events.json',(err, data)=>{
                    res.send({events:JSON.parse(data.toString())})
                })
            }

        })
    });
})

app.listen(8080,()=>{
    console.log('hi')
})